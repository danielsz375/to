package com.company;

import com.company.Message.Message;
import com.company.Message.TextMessage;
import com.company.Screenshot.Screenshot;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable{
    private int port;
    private String address;
    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;

    public Client(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public void run() {
        try {
            this.socket = new Socket(this.address, this.port);
            if(this.socket.isConnected()) {
                System.out.println("Klient " + this.socket.getLocalPort() + " jest połączony z serwerem.");
            }

            this.outputStream = new DataOutputStream(this.socket.getOutputStream());
            this.inputStream = new DataInputStream(this.socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            while(true) {
                ObjectInputStream objectInputStream = new ObjectInputStream(this.inputStream);
                Message requestMessage = (Message)objectInputStream.readObject();

                ObjectOutputStream objectOutputStream = new ObjectOutputStream(this.outputStream);

                requestMessage.handleRequest();
                objectOutputStream.writeObject(requestMessage);

                System.out.println("Klient " + this.socket.getLocalPort() + ": Wysłano odpowiedź!");
            }
        } catch (IOException | ClassNotFoundException e) {
            //e.printStackTrace();
            //Tu powinno być ywołanie close() na streamach i socket
        }
    }


}
