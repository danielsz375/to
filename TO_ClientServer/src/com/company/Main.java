package com.company;

import com.company.Server.Server;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Thread serverThread = new Thread(new Server(6789));
        serverThread.start();

        //Program nie obsługuje dwóch klientów
        for(int i = 0; i < 3; i++) {
            Thread clientThread = new Thread(new Client("localhost", 6789));
            clientThread.setName(Integer.toString(i));
            clientThread.start();
        }
    }
}
