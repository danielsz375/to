package com.company.Message;

import com.company.Screenshot.Screenshot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class BitmapMessage extends Message {
    private static int screenshotNumber = 0;
    public BitmapMessage() {
        this.messageEnum = MessageEnum.BMP;
    }

    public void handleResponse() {
        if(this.getMessageEnum() == MessageEnum.BMP) {
            try {
                String pathname = "C:\\screenshot" + screenshotNumber++ + ".bmp";
                ByteArrayInputStream input_stream = new ByteArrayInputStream(this.getContent());
                BufferedImage final_buffered_image = ImageIO.read(input_stream);
                ImageIO.write(final_buffered_image , "bmp", new File(pathname) );
                System.out.println("Screenshot został zapisany w lokalizacji: " + pathname);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            this.next.handleResponse();
        }
    }

    public void handleRequest() {
        if(this.getMessageEnum() == MessageEnum.BMP) {
            Screenshot screenshot = new Screenshot();
            this.setContent(screenshot.getScreenshotAsBytesArray());
        } else {
            this.next.handleRequest();
        }
    }
}
