package com.company.Message;

import java.io.Serializable;

public interface IMessage {

    public void moveForward();
    public abstract void handleResponse();
    public abstract void handleRequest();
    public MessageEnum getMessageEnum();
    public void setContent(byte[] content);
    public byte[] getContent();
}
