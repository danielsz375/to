package com.company.Message;

import java.io.Serializable;

public abstract class Message implements IMessage, Serializable {
    protected MessageEnum messageEnum;
    protected byte[] content;
    protected Message next;

    public void moveForward() {
        this.next.handleResponse();
    }

    public abstract void handleResponse();

    public abstract void handleRequest();

    public MessageEnum getMessageEnum() {
        return this.messageEnum;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getContent() {
        return this.content;
    }
}
