package com.company.Message;

import java.util.Scanner;

public class TextMessage extends Message{
    public TextMessage() {
        this.messageEnum = MessageEnum.TXT;
        this.next = new BitmapMessage();
    }

    public void handleResponse() {
        if(this.getMessageEnum() == MessageEnum.TXT) {
            System.out.println("Odczytany tekst:");
            String decodedResponseContent = new String(this.getContent());
            System.out.println(decodedResponseContent);
        } else {
            this.next.handleResponse();
        }
    }

    public void handleRequest() {
        if(this.getMessageEnum() == MessageEnum.TXT) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Wpisz tekst do wysłania na serwer: ");
            String textToSend = scanner.nextLine();
            byte[] textInBytes = textToSend.getBytes();
            this.setContent(textInBytes);
        } else {
            this.next.handleRequest();
        }
    }
}
