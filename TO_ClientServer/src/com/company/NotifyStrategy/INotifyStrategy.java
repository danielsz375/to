package com.company.NotifyStrategy;

import com.company.Message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.List;

public interface INotifyStrategy {
    public void sendToClients(List<Socket> clientSocketList, Message messageToSend);
    public void receiveResponse(Socket clientSocket);
}
