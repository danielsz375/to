package com.company.NotifyStrategy;

import com.company.Message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class MyRunnable implements Runnable {
    private Socket clientSocket;

    public MyRunnable(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        while(true) {
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());
                Message responseMessage = (Message)objectInputStream.readObject();
                responseMessage.handleResponse();
            } catch (IOException | ClassNotFoundException e ) {
                e.printStackTrace();
            }
        }
    }
}
