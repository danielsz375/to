package com.company.NotifyStrategy;

import com.company.Message.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class NotifyAll extends NotifyStrategy {

    public void sendToClients(List<Socket> clientSocketList, Message messageToSend) {
        for(Socket clientSocket : clientSocketList) {
            System.out.println("Wysyłam Request do klienta na porcie " + clientSocket.getPort());
            try {
                OutputStream outputStream = clientSocket.getOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(messageToSend);
                System.out.println("Wiadomość wysłana do klienta na porcie " + clientSocket.getPort() + ". Oczekiwanie na odpowiedź...");
                this.receiveResponse(clientSocket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}