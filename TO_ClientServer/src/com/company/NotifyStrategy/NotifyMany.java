package com.company.NotifyStrategy;

import com.company.Message.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class NotifyMany extends NotifyStrategy{
    public void sendToClients(List<Socket> clientSocketList, Message messageToSend) {
        Scanner scanner = new Scanner(System.in);
        int newPortNumber;
        Set<Integer> set = new TreeSet<Integer>();
        System.out.println("Powiadamianie wielu klientów. Podaj 0, aby zakończyć.");
        do {
            System.out.println("Podaj port klienta do powiadomienia:");
            newPortNumber = scanner.nextInt();
            set.add(newPortNumber);
        } while(newPortNumber != 0);

        for(int portNumber : set) {
            for(Socket clientSocket : clientSocketList) {
                if(clientSocket.getPort() == portNumber) {
                    System.out.println("Wysyłam Request do klienta na porcie " + clientSocket.getPort());
                    try {
                        OutputStream outputStream = clientSocket.getOutputStream();
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                        objectOutputStream.writeObject(messageToSend);
                        System.out.println("Wiadomość wysłana do klienta na porcie " + clientSocket.getPort() + ". Oczekiwanie na odpowiedź...");
                        this.receiveResponse(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

    }
}