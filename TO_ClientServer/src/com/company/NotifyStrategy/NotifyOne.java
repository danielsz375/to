package com.company.NotifyStrategy;

import com.company.Message.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class NotifyOne extends NotifyStrategy{
    public void sendToClients(List<Socket> clientSocketList, Message messageToSend) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj port klienta do powiadomienia:");
        int portNumber = scanner.nextInt();

        for(Socket clientSocket : clientSocketList) {
            if(clientSocket.getPort() == portNumber) {
                System.out.println("Wysyłam Request do klienta " + clientSocket.getPort());
                try {
                    OutputStream outputStream = clientSocket.getOutputStream();
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                    objectOutputStream.writeObject(messageToSend);
                    System.out.println("Wiadomość wysłana do klienta " + clientSocket.getPort() + ". Oczekiwanie na odpowiedź...");
                    this.receiveResponse(clientSocket);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}
