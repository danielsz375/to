package com.company.Server;

import com.company.Client;
import com.company.Message.*;
import com.company.NotifyStrategy.INotifyStrategy;
import com.company.NotifyStrategy.NotifyAll;
import com.company.NotifyStrategy.NotifyMany;
import com.company.NotifyStrategy.NotifyOne;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Server implements Runnable{
    private int port;
    private List<Socket> clientSocketList = new ArrayList<>();
    private ServerSocket serverSocket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private INotifyStrategy strategy;

    public Server(int port) { this.port = port; }

    public void displayClientList() {
        System.out.println("Obecna lista portów klientów: ");
        int i = 0;
        for (Socket clientSocket : clientSocketList) {
            System.out.println(" - " + clientSocket.getPort());
            i++;
        }
    }

    public void run() {
        try {
            //Uruchomienie serwera
            this.serverSocket = new ServerSocket(this.port);

            new Thread(new Runnable() {
                public void run() {
                    while(true) {
                        registerClient();
                    }
                }
            }).start();

            while(true) {
                //Oczekiwanie na klienta i jego rejestracja

                Message messageToSend = this.createAndGetRequest();

                System.out.println("Do ilu klientów wysłać Request?");
                System.out.println("[1] Jeden klient");
                System.out.println("[2] Wielu klientów");
                System.out.println("[3] Wszyscy klienci");
                Scanner scanner = new Scanner(System.in);
                int option = scanner.nextInt();
                switch(option) {
                    case 1: strategy = new NotifyOne(); break;
                    case 2: strategy = new NotifyMany(); break;
                    case 3: strategy = new NotifyAll(); break;
                    default: continue;
                }

                strategy.sendToClients(this.clientSocketList, messageToSend);
                //Thread.sleep(5000);
            }

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void registerClient() {
        try {
            Socket newClientSocket = this.serverSocket.accept();
            clientSocketList.add(newClientSocket);
            //System.out.println("Adres " + newClientSocket.getLocalAddress());
            //Tymczasowo jest to zrobione dla jednego klienta
            this.outputStream = newClientSocket.getOutputStream();
            this.inputStream = newClientSocket.getInputStream();


            System.out.println("Dodano nowego klienta!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Message createAndGetRequest() {
        IMessage message;
        Scanner scanner = new Scanner(System.in);
        char option;
        do {
            System.out.println("Wybierz opcję:");
            System.out.println("[1] - TEXT REQUEST");
            System.out.println("[2] - BMP REQUEST");
            option = scanner.nextLine().charAt(0);
            //tymczasowo
            displayClientList();
            switch(option) {
                case '1': return new TextMessage();
                case '2': return new BitmapMessage();
                default:
                    System.out.println("Wybrano niewłaściwą opcję!");
            }
        } while(option == '1' || option == '2');


        // Nie wiem co zrobić w tej sytuacji
        return null;
    }

}
