package TO_1;
 
public class Calculate {
   
    Currency currency1;
    Currency currency2;
    double value;
   
    Calculate(Currency c1, Currency c2) {
        this.currency1 = c1;
        this.currency2 = c2;
    }
 
    public void setValue(double value) {
        this.value = value;
    }
    
    public double calc()    {
       return value * this.currency1.getAvRate() * this.currency1.getConverter() / this.currency2.getAvRate() / this.currency2.getConverter();
    }
}