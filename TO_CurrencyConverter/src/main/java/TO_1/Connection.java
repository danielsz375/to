package TO_1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.net.URLConnection;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
/**
 *
 * @author Takedown
 */
public abstract class Connection {
    public static byte[] getResource(String url) {
        try {
            URL myURL = new URL("https://www.nbp.pl/kursy/xml/LastA.xml");
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            InputStream is = null;
            try {
              is = myURL.openStream();
              byte[] byteChunk = new byte[4096];
              
              int n;
              while ( (n = is.read(byteChunk)) > 0 ) {
                baos.write(byteChunk, 0, n);
              }

              return baos.toByteArray();
            }
            catch (IOException e) {
              System.err.printf ("Failed while reading bytes from %s: %s", myURL.toExternalForm(), e.getMessage());
              e.printStackTrace();
            }
            finally {
                if (is != null) { is.close(); }
            }
        } 
        catch (MalformedURLException e) { 
            System.out.println("Connection MalformedURLException");
            e.printStackTrace();
        } 
        catch (IOException e) { 
            System.out.println("Connection IOException");
            e.printStackTrace();
        }
        return null;
       
    } 
}
