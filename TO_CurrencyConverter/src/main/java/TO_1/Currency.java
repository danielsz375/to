package TO_1;
 
public class Currency {
    String name;
    int converter;
    String code;
    double avRate;
 
    Currency(String name, int converter, String code, double avRate)    {
        this.name = name;
        this.converter = converter;
        this.code = code;
        this.avRate = avRate;
    }
 
    public String getName() {
        return name;
    }
 
    public int getConverter() {
        return converter;
    }
 
    public String getCode() {
        return code;
    }
 
    public double getAvRate() {
        return avRate;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public void setConverter(int converter) {
        this.converter = converter;
    }
 
    public void setCode(String code) {
        this.code = code;
    }
 
    public void setAvRate(double avRate) {
        this.avRate = avRate;
    }
}