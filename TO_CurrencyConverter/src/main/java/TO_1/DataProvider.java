/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TO_1;

import org.w3c.dom.Document;
import java.util.*;

/**
 *
 * @author Takedown
 */
public class DataProvider implements IDataProvider {
    @Override
    public void requireRemoteData() throws Exception {
        byte[] data =  Connection.getResource("https://www.nbp.pl/kursy/xml/LastA.xml");
        Document xmlDoc = ParseXML.parse(data);
        Repository repo = NConverter.remoteRepository(xmlDoc);
        
        // Pobieranie danych od użytkownika
        Scanner myObj = new Scanner(System.in);
        System.out.println("Podaj kod waluty nr 1:");
        String cur1 = myObj.nextLine();
        System.out.println("Podaj ilość waluty nr 1:");
        String value = myObj.nextLine();
        System.out.println("Podaj kod waluty nr 2:");
        String cur2 = myObj.nextLine();
        
        UserCalc.uCalc(cur1, cur2, value, repo);
        
    }
}
