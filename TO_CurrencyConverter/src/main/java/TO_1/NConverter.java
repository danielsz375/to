package TO_1;

import org.w3c.dom.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Takedown
 */
public abstract class NConverter {
    public static Repository remoteRepository(Document xml) {
        xml.getDocumentElement().normalize();
        NodeList nList = xml.getElementsByTagName("pozycja");
        Repository repo = new Repository();
        
        for(int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
           
            
            if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                repo.addCurrency(new Currency(
                        eElement.getElementsByTagName("nazwa_waluty").item(0).getTextContent(),
                        Integer.parseInt(eElement.getElementsByTagName("przelicznik").item(0).getTextContent()),
                        eElement.getElementsByTagName("kod_waluty").item(0).getTextContent(),
                        Double.parseDouble(eElement.getElementsByTagName("kurs_sredni").item(0).getTextContent().replace(",", "."))
                ));
            }
        }
        
        return repo;
    }
}
