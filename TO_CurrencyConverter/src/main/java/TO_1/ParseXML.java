/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TO_1;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;
import org.w3c.dom.*;
import javax.xml.parsers.*;
/**
 *
 * @author Takedown
 */
public abstract class ParseXML {
    public static Document parse(byte[] tab) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        Document docXml = builder.parse(new ByteArrayInputStream(tab));
        
        return docXml;
    }
    
    
}
