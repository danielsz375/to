/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TO_1;
import java.util.*;
/**
 *
 * @author Takedown
 */
public class PrintCurrencyList {
    public void print(Repository repo) {
        List<Currency> list = repo.getList();
        for(Currency currency: list) {
            System.out.println(currency.getName());
            System.out.println(currency.getConverter());
            System.out.println(currency.getCode());
            System.out.println(currency.getAvRate());
        }
    }
}
