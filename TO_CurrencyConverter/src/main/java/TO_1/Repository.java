/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TO_1;

import java.util.*;
/**
 *
 * @author Takedown
 */


public class Repository {
    private List<Currency> currencyList;
    
    public Repository() {
        this.currencyList = new ArrayList<>();
        this.addCurrency(
                new Currency(
                        "złoty polski",
                        1,
                        "PLN",
                        1.0
                )
        );
    }
    
    public List getList() {
        return currencyList;
    }
    
    public void addCurrency(Currency currency) {
        this.currencyList.add(currency);
    }
    
    public Currency getValueByCode(String currencyCode) {
        for(Currency currency: this.currencyList) {

            if(currency.getCode().equals(currencyCode)) {
                return currency;
            }
        }
        return null;
    }
    
    
}
