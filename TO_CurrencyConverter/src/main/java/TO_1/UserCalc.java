/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TO_1;
import java.util.Scanner; 
/**
 *
 * @author Takedown
 */
public abstract class UserCalc {
    public static void uCalc(String cur1, String cur2, String value, Repository repo){
        Currency currency1 = repo.getValueByCode(cur1);
        Currency currency2 = repo.getValueByCode(cur2);
        
        Calculate calculator = new Calculate(currency1, currency2);   
        calculator.setValue(Double.parseDouble(value.replace(",", ".")));
        
        System.out.println(value + " " + cur1 + " = " + calculator.calc() + " " + cur2);
    }
}
