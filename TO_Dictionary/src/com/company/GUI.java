package com.company;

import com.company.Letter.LetterCollection;
import com.company.Word.WordFinder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

public class GUI extends JFrame {
    private JTextField t;
    private JPanel mainPanel;
    private JPanel labelsPanel;

    private LetterCollection root;

    public GUI(LetterCollection root) {
        this.root = root;

        t = new JTextField("", 27);
        t.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                //Updating labels
                updateLabelsPanel();
            }
        });

        this.labelsPanel = new JPanel();
        mainPanel = new JPanel();

        mainPanel.add(t);
        mainPanel.add(labelsPanel);

        updateLabelsPanel();

        setTitle("Słownik");
        setLayout(new CardLayout());
        add(mainPanel);
        setSize(300, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public String getTextFieldValue() {
        return this.t.getText();
    }

    public void updateLabelsPanel() {
        labelsPanel = new JPanel();
        labelsPanel.setLayout(new BoxLayout(labelsPanel, BoxLayout.PAGE_AXIS));

        String text = getTextFieldValue();
        if(text.length() < 2) {
            return;
        }

        List<String> list;
        WordFinder wordFinder = new WordFinder(this.root);
        list = wordFinder.getListUpToSixWords(text);
        int i = 0;
        for(String s : list) {
            JLabel l = new JLabel(s);
            labelsPanel.add(l);
            if(i == 5) {
                break;
            }
        }

        mainPanel.remove(1);
        mainPanel.add(labelsPanel);
        labelsPanel.revalidate();
        repaint();
    }
}
