package com.company.Letter.Character;

public class Character {
    protected char character;

    public Character(char character) {
        this.character = character;
    }

    public char getCharValue() {
        return this.character;
    }

    @Override
    public String toString() {
        String s = java.lang.Character.toString(this.character);
        return s;
    }

}
