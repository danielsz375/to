package com.company.Letter.Character;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharacterFlyweight {
    public List<Character> characters = new ArrayList<>();
    public Map<java.lang.Character, Integer> occurences = new HashMap<>();

    public Character getCharacter(char key) {

        Character character;
        for(Character existingCharacter : characters) {
            if(existingCharacter.getCharValue() == key) {
                int count = occurences.containsKey(key) ? occurences.get(key) : 1;
                occurences.put(key, count + 1);
                character = existingCharacter;
                return character;
            }
        }

        character = new Character(key);
        characters.add(character);

        return character;
    }

    public void displayOccurences() {
        for(Map.Entry<java.lang.Character, Integer> entry : occurences.entrySet()) {
            System.out.println("Litera: " + entry.getKey() + " Wystąpienia: " + entry.getValue());
        }
    }
}
