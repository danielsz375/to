package com.company.Letter;

import com.company.Letter.Character.Character;
import com.company.Letter.Character.CharacterFlyweight;

public class Letter {
    private Character character;
    private CharacterFlyweight characterFlyweight;
    private LetterCollection nextLetterCollection;

    public Letter(Character character, CharacterFlyweight characterFlyweight) {
        this.character = character;
        this.characterFlyweight = characterFlyweight;
        this.nextLetterCollection = new LetterCollection(this.characterFlyweight);
    }

    public char getCharValue() {
        return this.character.getCharValue();
    }

    public LetterCollection getNextLetterCollection() {
        return this.nextLetterCollection;
    }

}
