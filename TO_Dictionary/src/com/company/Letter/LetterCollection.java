package com.company.Letter;

import com.company.Letter.Character.Character;
import com.company.Letter.Character.CharacterFlyweight;

import java.util.ArrayList;
import java.util.List;

public class LetterCollection {
    private CharacterFlyweight characterFlyweight;
    private List<Letter> list = new ArrayList<Letter>();

    public LetterCollection(CharacterFlyweight characterFlyweight) {
        this.characterFlyweight = characterFlyweight;
    }

    public Letter mustGetLetter(Character key) {
        //Zwraca literę z nextLetterList, a jeśli litera o podanym znaku nie istnieje to ją tworzy, dodaje do zbioru
        Letter letterToReturn = null;

        for(Letter l : this.list) {
            if(l.getCharValue() == key.getCharValue()) {
                letterToReturn = l;
            }
        }

        if(letterToReturn == null) {
            letterToReturn = new Letter(characterFlyweight.getCharacter(key.getCharValue()), characterFlyweight);
            list.add(letterToReturn);
        }
        return letterToReturn;
    }

    public Letter mustGetLetter(char key) {
        //Zwraca literę z nextLetterList, a jeśli litera o podanym znaku nie istnieje to ją tworzy, dodaje do zbioru
        Letter letterToReturn = null;

        for(Letter l : this.list) {
            if(l.getCharValue() == key) {
                letterToReturn = l;
            }
        }

        if(letterToReturn == null) {
            letterToReturn = new Letter(characterFlyweight.getCharacter(key), characterFlyweight);
            list.add(letterToReturn);
        }
        return letterToReturn;
    }

    public ArrayList<String> createAndGetListOfWords(String key) {
        ArrayList<String> listToReturn = new ArrayList<String>();

        //Przypadek elementarny
        if(this.list.size() < 1) {
            listToReturn.add(key);
            return listToReturn;
        }

        for(Letter letter : this.list) {
            ArrayList<String> temp = letter.getNextLetterCollection().createAndGetListOfWords(key + String.valueOf(letter.getCharValue()));
            if(temp != null) listToReturn.addAll(temp);
        }

        //Konstruowanie listy słów, przez doklejanie potencjalnych liter do wyszukiwanego słowa
        for(String word : listToReturn) {
            word = key + word;
        }

        return listToReturn;
    }

    public CharacterFlyweight getCharacterFlyweight() {
        return this.characterFlyweight;
    }
 }