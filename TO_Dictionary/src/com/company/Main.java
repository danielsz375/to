package com.company;

import com.company.Letter.LetterCollection;
import com.company.Word.WordImporter;

public class Main {

    public static void main(String[] args) {
        WordImporter wordImporter = new WordImporter("slowa.txt");
        LetterCollection root = wordImporter.createAndGetBaseLetterCollection();
        GUI gui = new GUI(root);
        root.getCharacterFlyweight().displayOccurences();
    }
}
