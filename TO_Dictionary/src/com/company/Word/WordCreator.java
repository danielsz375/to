package com.company.Word;

import com.company.Letter.Character.CharacterFlyweight;
import com.company.Letter.Letter;
import com.company.Letter.LetterCollection;

public class WordCreator {
    private LetterCollection baseLetterCollection;
    private CharacterFlyweight characterFlyweight;

    public WordCreator(LetterCollection baseLetterCollection, CharacterFlyweight characterFlyweight) {
        this.baseLetterCollection = baseLetterCollection;
        this.characterFlyweight = characterFlyweight;
    }

    public void create(String word) {
        if(word.length() > 0) {
            WordIterator wordIterator = new WordIterator(word, characterFlyweight);

            Letter letter = baseLetterCollection.mustGetLetter(wordIterator.getNext());
            while(wordIterator.hasNext()) {
                letter = letter.getNextLetterCollection().mustGetLetter(wordIterator.getNext());
            }
        } else {
            System.out.println("Słowo o długości 0.");
        }

    }
}
