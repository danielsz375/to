package com.company.Word;

import com.company.Letter.Letter;
import com.company.Letter.LetterCollection;

import java.util.ArrayList;
import java.util.List;

public class WordFinder {
    private LetterCollection root;

    public WordFinder(LetterCollection root) {
        this.root = root;
    }

    public List<String> getListUpToSixWords(String key) {
        Letter l = root.mustGetLetter(key.charAt(0));

        if(key.length() > 1) {
            for(int i = 1; i < key.length(); ++i) {
                l = l.getNextLetterCollection().mustGetLetter(key.charAt(i));
            }
        }

        ArrayList<String> lista = l.getNextLetterCollection().createAndGetListOfWords(key);
        if(lista != null) {
            if(lista.size() > 6) {
                return lista.subList(0, 6);
            }
        } else {
            //Dodawanie nowego słowa do listy
            lista = new ArrayList<String>();
            lista.add(key);
        }

        return lista;
    }
}
