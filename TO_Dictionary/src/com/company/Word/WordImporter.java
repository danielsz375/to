package com.company.Word;


import com.company.Letter.Character.CharacterFlyweight;
import com.company.Letter.LetterCollection;
import com.company.Word.WordCreator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;

public class WordImporter {
    private File file;
    private Scanner scanner;

    public WordImporter(String filename) {
        try {
            Locale locale = new Locale("pl","PL");
            file = new File(filename);
            scanner = new Scanner(file, "UTF-8");
            scanner.useLocale(locale);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public LetterCollection createAndGetBaseLetterCollection() {
        String newWord;
        CharacterFlyweight characterFlyweight = new CharacterFlyweight();
        LetterCollection root = new LetterCollection(characterFlyweight);
        WordCreator wordCreator = new WordCreator(root, characterFlyweight);
        int x = 0;
        while(scanner.hasNext()) {
            newWord = scanner.nextLine();
            wordCreator.create(newWord);
        }
        return root;
    }
}
