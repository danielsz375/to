package com.company.Word;

import com.company.Letter.Character.Character;
import com.company.Letter.Character.CharacterFlyweight;

import java.util.ArrayList;
import java.util.List;

public class WordIterator {
    private CharacterFlyweight characterFlyweight;
    private Character[] characters;
    private int index = -1;

    public WordIterator(String word, CharacterFlyweight characterFlyweight) {
        this.characterFlyweight = characterFlyweight;
        characters = new Character[word.length()];
        for(int i = 0; i < word.length(); i++) {
            characters[i] = characterFlyweight.getCharacter(word.charAt(i));
        }
    }

    public boolean hasNext() {
        if(index < characters.length - 1) {
            return true;
        }
        return false;
    }

    public Character getNext() {
        if(hasNext()) {
            return characters[++index];
        }
        return null;
    }
}
