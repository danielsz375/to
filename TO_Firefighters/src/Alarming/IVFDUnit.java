/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alarming;

import Alarming.UnitState.IUnitState;

/**
 *
 * @author retsu
 */
public interface IVFDUnit {
    public ResponseCode notify(String CCIR_CODE);
    public void setUnitState(IUnitState siren);
    public String getUnitName();
}
