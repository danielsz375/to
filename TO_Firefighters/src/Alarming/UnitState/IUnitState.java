package Alarming.UnitState;

import Alarming.IVFDUnit;

public interface IUnitState {
    public void changeState(IVFDUnit context);

}
