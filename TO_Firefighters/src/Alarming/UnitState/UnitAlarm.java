package Alarming.UnitState;

import Alarming.IVFDUnit;

public class UnitAlarm implements IUnitState {
    @Override
    public void changeState(IVFDUnit context) {
        context.setUnitState(new UnitReady());
        System.out.println("Syrena alarmowa została wyłączona.");
    }
}
