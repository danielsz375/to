package Alarming.UnitState;

import Alarming.IVFDUnit;

public class UnitReady implements IUnitState {
    @Override
    public void changeState(IVFDUnit context) {
        context.setUnitState(new UnitAlarm());
        System.out.println("Syrena alarmowa w jednostce " + context.getUnitName() + " została włączona.");
    }
}
