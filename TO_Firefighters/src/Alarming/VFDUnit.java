/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alarming;

import Alarming.UnitState.IUnitState;
import Alarming.UnitState.UnitReady;
import Firefighters.Firefighter;
import Firefighters.IFirefighter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author retsu
 */
public class VFDUnit implements IVFDUnit {
    List<IFirefighter> firefighterList = new ArrayList<>();
    IUnitState siren = new UnitReady();

    String unitName;
    String testCode;
    String alarmCode;

    public VFDUnit(String unitName, String testCode, String alarmCode) {
        this.unitName = unitName;
        this.testCode = testCode;
        this.alarmCode = alarmCode;
    }

    public void setUnitState(IUnitState siren) {
        this.siren = siren;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public String getAlarmCode() {
        return alarmCode;
    }

    public void setAlarmCode(String alarmCode) {
        this.alarmCode = alarmCode;
    }

    public void registerFirefighter(IFirefighter firefighter) {
        this.firefighterList.add(firefighter);
    }

    public void unregisterFirefighter(IFirefighter firefighter) {
        this.firefighterList.remove(firefighter);
    }

    @Override
    public ResponseCode notify(String CCIR_CODE) {
        if(CCIR_CODE.equals(this.alarmCode)) {
            for(IFirefighter firefighter : this.firefighterList) {
                if(siren instanceof UnitReady) {
                    siren.changeState(this);
                }
                firefighter.sendSms(this.alarmCode);
            }
            return ResponseCode.ALARM_OK;
        } else if (CCIR_CODE.equals(this.testCode)) {
            if(siren instanceof UnitReady) {
                siren.changeState(this);
            }
            // Czy test ma wyglądać tak jak alarm?
            return ResponseCode.TEST_OK;
        }
        return ResponseCode.ERROR;
    }
    
}
