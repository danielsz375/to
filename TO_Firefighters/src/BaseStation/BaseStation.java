package BaseStation;

import Alarming.IVFDUnit;
import Alarming.ResponseCode;
import Alarming.VFDUnit;
import BaseStation.CommunicateState.AlarmCommunicate;
import BaseStation.CommunicateState.ICommunicateState;
import BaseStation.CommunicateState.TestCommunicate;
import Firefighters.Firefighter;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BaseStation implements IBaseStation {
    List<IVFDUnit> vfdUnits = new ArrayList<>();
    ICommunicateState state = new AlarmCommunicate();
    INotifyStrategy strategy;

    public BaseStation(INotifyStrategy strategy) {
        this.strategy = strategy;
    }

    public void registerUnit(IVFDUnit newUnit) {
        this.vfdUnits.add(newUnit);
    }

    public void unregisterUnit(IVFDUnit oldUnit) {
        this.vfdUnits.remove(oldUnit);
    }

    public void setState(ICommunicateState newState) {
        this.state = newState;
    }

    public ICommunicateState getState() {
        return this.state;
    }

    @Override
    public void notifyUnits(String code) {
        strategy.notifyAlgorithm(vfdUnits, code);
    }

    public void communicateSettings() {
        System.out.println("Obecnie wybrany typ powiadomienia: ");
        if(this.state instanceof AlarmCommunicate) {
            System.out.println("ALARM");
        } else if (this.state instanceof TestCommunicate) {
            System.out.println("TEST");
        } else {
            System.out.println("UNKNOWN");
        }

        Scanner obj = new Scanner(System.in);
        int option = 0;
        do {
            System.out.println("[1] - Kontynuuj");
            System.out.println("[2] - Zmień typ powiadomienia");
            option = obj.nextInt();

            if(option == 2)
                this.state.changeState(this);
        } while (option != 1);


    }

    public void run() {
        communicateSettings();


        //scenariusz
        VFDUnit vfdUnit = new VFDUnit("unit1", "TestCommunicate", "AlarmCommunicate");
        this.registerUnit(vfdUnit);
        VFDUnit vfdUnit2 = new VFDUnit("unit2", "TestCommunicate", "AlarmCommunicate");
        this.registerUnit(vfdUnit2);
        VFDUnit vfdUnit3 = new VFDUnit("unit3", "TestCommunicate", "AlarmCommunicate");
        this.registerUnit(vfdUnit3);
        VFDUnit vfdUnit4 = new VFDUnit("unit4", "TestCommunicate", "AlarmCommunicate");
        this.registerUnit(vfdUnit4);

        Firefighter firefighter = new Firefighter("u1", "f1", "11");
        Firefighter firefighter2 = new Firefighter("u1", "f2", "12");
        Firefighter firefighter3 = new Firefighter("u1", "f3", "13");
        Firefighter firefighter4 = new Firefighter("u2", "f1", "21");
        Firefighter firefighter5 = new Firefighter("u2", "f2", "22");
        Firefighter firefighter6 = new Firefighter("u3", "f1", "31");
        Firefighter firefighter7 = new Firefighter("u4", "f1", "41");
        Firefighter firefighter8 = new Firefighter("u4", "f2", "42");

        vfdUnit.registerFirefighter(firefighter);
        vfdUnit.registerFirefighter(firefighter2);
        vfdUnit.registerFirefighter(firefighter3);
        vfdUnit2.registerFirefighter(firefighter4);
        vfdUnit2.registerFirefighter(firefighter5);
        vfdUnit3.registerFirefighter(firefighter6);
        vfdUnit4.registerFirefighter(firefighter7);
        vfdUnit4.registerFirefighter(firefighter8);


        this.notifyUnits(this.state.getClass().getSimpleName());
    }
}
