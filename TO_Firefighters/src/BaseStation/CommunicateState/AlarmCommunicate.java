package BaseStation.CommunicateState;

import BaseStation.BaseStation;

public class AlarmCommunicate implements ICommunicateState{
    @Override
    public void changeState(BaseStation context) {
        context.setState(new TestCommunicate());
        System.out.println("Obecnie wybrany typ powiadomienia: TEST");
    }
}
