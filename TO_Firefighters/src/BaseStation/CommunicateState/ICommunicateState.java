package BaseStation.CommunicateState;

import BaseStation.BaseStation;

public interface ICommunicateState {
    public void changeState(BaseStation context);
}
