package BaseStation.CommunicateState;

import BaseStation.BaseStation;

public class TestCommunicate implements ICommunicateState {
    @Override
    public void changeState(BaseStation context) {
        context.setState(new AlarmCommunicate());
        System.out.println("Obecnie wybrany typ powiadomienia: ALARM");
    }
}
