package BaseStation;

import Alarming.IVFDUnit;

import java.util.ArrayList;
import java.util.List;

public interface IBaseStation {
    public void registerUnit(IVFDUnit newUnit);
    public void unregisterUnit(IVFDUnit oldUnit);
    public void notifyUnits(String code);
}
