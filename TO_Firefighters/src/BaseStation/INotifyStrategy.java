package BaseStation;

import Alarming.IVFDUnit;

import java.util.List;

public interface INotifyStrategy {
    public void notifyAlgorithm(List<IVFDUnit> vfdUnits, String code);
}
