package BaseStation;

import Alarming.IVFDUnit;
import Alarming.ResponseCode;

import java.util.List;
import java.util.Scanner;

public class NotifyManyUnits implements INotifyStrategy {
    public void notifyAlgorithm(List<IVFDUnit> vfdUnits, String code) {
        System.out.println("Tryb powiadamiania wielu jednostek.");
        System.out.println("Aby zakończyć powiadamianie jednostek wpisz end:");
        Scanner obj = new Scanner(System.in);
        String unitName;
        ResponseCode response;

        do {
            System.out.println("Podaj nazwę jednostki do powiadomienia:");
            unitName = obj.nextLine();
            for (IVFDUnit vfdUnit: vfdUnits) {
                if(vfdUnit.getUnitName().equals(unitName)) {
                    response = vfdUnit.notify(code);
                    System.out.println("Stacja bazowa powiadomiła jednostkę " + vfdUnit.getUnitName());
                    System.out.println("Otrzymana odpowiedź: " + response);
                    break;
                }
            }
        } while(!unitName.equals("end"));

        System.out.println("Zakończono powiadamianie jednostek.");
    }
}
