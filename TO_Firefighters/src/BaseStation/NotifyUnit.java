package BaseStation;

import Alarming.IVFDUnit;
import Alarming.ResponseCode;

import java.util.List;
import java.util.Scanner;

public class NotifyUnit implements INotifyStrategy {
    public void notifyAlgorithm(List<IVFDUnit> vfdUnits, String code) {
        System.out.println("Podaj nazwę jednostki, która ma zostać powiadomiona:");
        Scanner obj = new Scanner(System.in);
        String unitName = obj.nextLine();
        ResponseCode response;

        for (IVFDUnit vfdUnit: vfdUnits) {
            if(vfdUnit.getUnitName().equals(unitName)) {
                response = vfdUnit.notify(code);
                System.out.println("Stacja bazowa powiadomiła jednostkę " + vfdUnit.getUnitName());
                System.out.println("Otrzymana odpowiedź: " + response);
                break;
            }
        }
    }
}
