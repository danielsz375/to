/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import Alarming.VFDUnit;
import BaseStation.*;
import Firefighters.Firefighter;

import java.util.Scanner;

/**
 *
 * @author retsu
 */
public class Lab5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Wybierz ilość jednostek do powiadomienia:");
        System.out.println("[1] - Powiadomienie jednej jednostki");
        System.out.println("[2] - Powiadomienie wielu jednostek");

        Scanner obj = new Scanner(System.in);
        int mode = obj.nextInt();


        if(mode == 1) {
            BaseStation baseStation = new BaseStation(new NotifyUnit());
            baseStation.run();
        } else if (mode == 2) {
            BaseStation baseStation = new BaseStation(new NotifyManyUnits());
            baseStation.run();
        }


    }
    
}
