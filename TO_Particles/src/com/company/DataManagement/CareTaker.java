package com.company.DataManagement;

import com.company.Movable.MovableData;

import java.util.ArrayList;
import java.util.List;

public class CareTaker {
    private List<MovableData> movableDataList;

    public CareTaker() {
        this.movableDataList = new ArrayList<>();
    }

    public void addMovableData(MovableData movableData) {
        this.movableDataList.add(movableData);
    }

    public List<MovableData> getMovableDataList() {
        return this.movableDataList;
    }

}
