package com.company.DataManagement;

import com.company.Movable.MovableData;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class DataLoader {
    private String filename;

    public DataLoader(String filename) {
        this.filename = filename;
    }

    public List<MovableData> loadData() {
        FileInputStream fis;
        ObjectInputStream ois;
        List<MovableData> movableDataList = null;
        try {
            fis = new FileInputStream("C:\\" + filename);
            ois = new ObjectInputStream(fis);
            movableDataList = (List<MovableData>) ois.readObject();
            ois.close();
        } catch (IOException e) {
            System.out.println("File with saved data doesn't exist.");
        } catch (Exception e ) {
            e.printStackTrace();
        }
        return movableDataList;
    }
}
