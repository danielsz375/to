package com.company.DataManagement;

import com.company.Movable.Movable;
import com.company.Movable.MovableData;

import javax.imageio.stream.FileImageOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class DataSaver {
    private String filename;

    public DataSaver(String filename) {
        this.filename = filename;
    }

    public void saveData(List<MovableData> movableDataList) {
        ObjectOutputStream oos;
        FileOutputStream fout;
        try {
            fout = new FileOutputStream("C:\\" + filename);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(movableDataList);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
