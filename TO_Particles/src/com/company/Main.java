package com.company;

import com.company.DataManagement.CareTaker;
import com.company.DataManagement.DataLoader;
import com.company.DataManagement.DataSaver;
import com.company.Movable.Movable;
import com.company.Movable.MovableData;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Particles");
                CareTaker careTaker = new CareTaker();
                Surface surface = new Surface(650);

                try {
                    DataLoader dataLoader = new DataLoader("data");
                    List<MovableData> movableDataList = dataLoader.loadData();
                    if(movableDataList.size() != 0) {
                        for(MovableData movableData : movableDataList) {
                            Movable movable = new Movable(new Vector2D(0, 0), new Vector2D(0,0));
                            movable.recoverState(movableData);
                            surface.addMovable(movable);
                        }
                    } else throw new IOException();
                } catch (Exception e) {
                    RandomGenerator rg = new RandomGenerator();
                    for(int i = 0; i < 8; i++) {
                        surface.addMovable(new Movable(new Vector2D(rg.generateInRange(5, 645), rg.generateInRange(5, 645)), new Vector2D(rg.generateOne(), rg.generateOne())));
                    }
                }

                frame.add(surface);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                frame.addWindowListener(new WindowAdapter(){
                    public void windowClosing(WindowEvent e){
                        List<Movable> movableList = surface.getMovables();
                        for(Movable movable : movableList) {
                            careTaker.addMovableData(movable.createData());
                        }
                        DataSaver dataSaver = new DataSaver("data");
                        dataSaver.saveData(careTaker.getMovableDataList());

                    }
                });
            }
        });
    }
}
