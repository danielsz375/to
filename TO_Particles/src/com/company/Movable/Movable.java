package com.company.Movable;

import com.company.Surface;
import com.company.Vector2D;

import java.awt.*;

public class Movable {
    private Vector2D position;
    private Vector2D velocity;
    private Surface surface;
    private float radius;

    public Movable(Vector2D position, Vector2D velocity) {
        this.position = position;
        this.velocity = velocity;
        this.radius = 10;
    }

//    public Movable(MovableData movableData) {
//        this.position = movableData.getPosition();
//        this.velocity = movableData.getVelocity();
//        this.radius = 10;
//    }

    public MovableData createData() {
        return new MovableData(this.position, this.velocity);
    }

    public void recoverState(MovableData movableData) {
        this.position = movableData.getPosition();
        this.velocity = movableData.getVelocity();
        this.radius = 10;
    }

    public void move() {
        if(this.position.getvX() < surface.getSurfaceSize() && this.position.getvX() > radius) {
            this.position.setvX(this.position.getvX() + this.velocity.getvX());
        }   else    {
            this.velocity.setvX(-this.velocity.getvX());
            this.position.setvX(this.position.getvX() + this.velocity.getvX());
        }

        if(this.position.getvY() < surface.getSurfaceSize() && this.position.getvY() > radius) {
            this.position.setvY(this.position.getvY() + this.velocity.getvY());;
        }   else    {
            this.velocity.setvY(-this.velocity.getvY());
            this.position.setvY(this.position.getvY() + this.velocity.getvY());;
        }

    }


    public void drawMovable(Graphics g) {
        g.setColor(Color.BLACK);

        g.fillOval((int)(position.getvX() - radius), (int)(position.getvY() - radius), (int)radius * 2, (int)radius * 2);
    }

    public Vector2D getPosition() {
        return position;
    }

    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }

    public Vector2D getVelocity() {
        return velocity;
    }

    public float getRadius() {
        return radius;
    }

    public void colliding(Movable movable)
    {
        double xd = this.position.getvX()  - movable.position.getvX();
        double yd = this.position.getvY()  - movable.position.getvY();

        double sumRadius = this.radius + movable.getRadius();
        double sqrRadius = sumRadius * sumRadius;

        double distSqr = (xd * xd) + (yd * yd);

        if (distSqr <= sqrRadius)
        {
            Vector2D velocityOne = movable.getVelocity();
            Vector2D velocityTwo = this.getVelocity();
            movable.setVelocity(velocityTwo);
            this.setVelocity(velocityOne);
//            this.velocity.setvX(-this.velocity.getvX());
//            this.velocity.setvY(-this.velocity.getvY());
//            movable.velocity.setvX(-movable.velocity.getvX());
//            movable.velocity.setvY(-movable.velocity.getvY());
        }
    }

    public Surface getSurface() {
        return surface;
    }

    public void setSurface(Surface surface) {
        this.surface = surface;
    }

}
