package com.company.Movable;

import com.company.Vector2D;

import java.io.Serializable;

public class MovableData implements Serializable {
    private Vector2D position;
    private Vector2D velocity;

    public MovableData(Vector2D position, Vector2D velocity) {
        this.position = position;
        this.velocity = velocity;
    }

    public Vector2D getPosition() {
        return position;
    }

    public void setPosition(Vector2D position) {
        this.position = position;
    }

    public Vector2D getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }
}
