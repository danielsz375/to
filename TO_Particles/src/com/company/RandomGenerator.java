package com.company;

import java.util.Random;

public class RandomGenerator {
    public int generateInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public double generateOne() {
        if(Math.random() < 0.5) {
            return 0.5;
        }
        return -0.5;
    }
}
