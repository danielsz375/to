package com.company;

import com.company.Movable.Movable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Surface extends JPanel {
    private int surfaceSize;
    private List<Movable> movables;


    public Surface(int surfaceSize) {
        this.surfaceSize = surfaceSize;
        this.movables = new ArrayList<>();


        Timer timer = new Timer(15, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(Movable movable : movables) {
                    nextIteration();
                    repaint();
                }
            }
        });
        timer.start();
    }

    public List<Movable> getMovables() {
        return this.movables;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(Movable movable : movables) {
            movable.drawMovable(g);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(surfaceSize, surfaceSize);
    }

    public void nextIteration() {
        for (int i = 0; i < movables.size(); i++)
        {
            for (int j = i + 1; j < movables.size(); j++)
            {
                movables.get(i).colliding(movables.get(j));
            }
        }

        for(Movable movable : movables) {
            movable.move();
        }
    }

    public void addMovable(Movable movable) {
        movable.setSurface(this);
        this.movables.add(movable);
    }

    public int getSurfaceSize() {
        return this.surfaceSize;
    }
}
