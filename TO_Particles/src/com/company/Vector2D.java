package com.company;

import java.io.Serializable;

public class Vector2D implements Serializable {
    private double vX;
    private double vY;

    public Vector2D(double vX, double vY) {
        this.vX = vX;
        this.vY = vY;
    }

    public double getvX() {
        return vX;
    }

    public void setvX(double vX) {
        this.vX = vX;
    }

    public double getvY() {
        return vY;
    }

    public void setvY(double vY) {
        this.vY = vY;
    }

}
