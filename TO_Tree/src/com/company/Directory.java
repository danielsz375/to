package com.company;
import java.util.*;

public class Directory extends Node {
    List<INode> iNodeList;

    public Directory() {
        //super();
        iNodeList = new ArrayList<>();
        this.setName("root");
        this.setParent(this);
        System.out.println("Root directory has been created.");
    }

    public Directory(String name, Directory parent) {
        super(name, parent);
        iNodeList = new ArrayList<>();
    }

    public void tree() {
        this.tree(0);
    }

    private void tree(int i) {
        System.out.println(this.getName());

        for(INode node : iNodeList) {
            for(int j = 0; j <= i; j++) {
                System.out.print("\t");
            }
            System.out.print(" - ");
            if(node instanceof Directory) {
                ((Directory)node).tree(i + 1);
            } else {
                System.out.println(node.getName());
            }
        }
    }

    public void add(INode node) {
        this.iNodeList.add(node);
        node.setParent(this);
    }

    public void remove(INode node) {
        this.iNodeList.remove(node);
    }

    public void move(INode node, Directory newParent) {
        this.remove(node);
        newParent.add(node);
    }

    public List<INode> getiNodeList() {
        return this.iNodeList;
    }
}
