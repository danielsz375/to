package com.company;

public class File extends Node {
    private String content;

    public File(String name, Directory parent, String content) {
        super(name, parent);
        this.content = content;
    }

    public void print() {
        System.out.println(content);
    }
}
