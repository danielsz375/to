package com.company;

public interface INode {
    public void setName(String name);
    public String getName();
    public void setParent(Directory node);
    public Directory getParent();

}
