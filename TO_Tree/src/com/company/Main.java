package com.company;

public class Main {

    public static void main(String[] args) {
        Directory rootNode = new Directory();
        Directory dir1 = new Directory("dir1", rootNode);
        Directory dir2 = new Directory("dir2", rootNode);
        Directory dir3 = new Directory("dir3", rootNode);
        Directory dir4 = new Directory("dir4", dir2);
        File file1 = new File("file1", rootNode, "");
        File file2 = new File("file2", dir2, "");
        File file3 = new File("file3", dir3, "");

        rootNode.tree();
        Verify v = new Verify(file2);
        v.setName("xDDDD");
        rootNode.tree();
        v.setName("xDDDD!!!");
        rootNode.tree();
        v.setName("xDDDDefujdpoiuiospdufjiufpweufrweoipufioew");
        rootNode.tree();
        v.setName("");
        rootNode.tree();
        v.setName("dir4");
        rootNode.tree();
    }
}
