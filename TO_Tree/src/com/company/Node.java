package com.company;

public class Node implements INode{
    private Directory parent;
    private String name;

    public Node(){
        this.setName("root");
    }

    public Node(String name, Directory parent) {
        this.setName(name);
        parent.add(this);
        System.out.println("Created " + name + ". Its parent: " + this.getParent().getName());
    }

    @Override
    public Directory getParent() {
        return parent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setParent(Directory parent) {
        this.parent = parent;
    }
}
