package com.company;

public class Verify implements INode {
    INode node;

    public Verify(INode node) {
        if (node != null) {
            this.node = node;
        }
    }

    @Override
    public void setName(String name) {
        if(name.length() > 1 && name.length() <= 16) {
            Directory parentDir = node.getParent();
            for(INode node : parentDir.getiNodeList()) {
                if(name.equals(node.getName())) {
                    System.out.println("Błąd: Obiekt o tej nazwie już istnieje!");
                    return;
                }
            }
            if(name.matches("[A-Za-z0-9]+")) {
                System.out.println("Zmieniam nazwę pliku " + node.getName() + " na " + name);
                node.setName(name);
                return;
            } else System.out.println("Błąd: Nazwa zawiera niedozwolone znaki!");
        } else System.out.println("Błąd: Niewłaściwa długość nowej nazwy!");
    }

    @Override
    public String getName() {
        return node.getName();
    }

    @Override
    public void setParent(Directory node) {
        node.setParent(node);
    }

    @Override
    public Directory getParent() {
        return node.getParent();
    }
}
