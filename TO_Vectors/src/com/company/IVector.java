package com.company;

public interface IVector {
    public double getAbs();
    public double getVx();
    public double getVy();
    public void setVx(double vx);
    public void setVy(double vy);
    public void displayCords();


    static double dotProduct(IVector a, IVector b) {

        double dot = (a.getVx() * b.getVx() + a.getVy() * b.getVy());

        if (a instanceof Vector3D && b instanceof Vector3D)    {
            dot += (((Vector3D)a).getVz() * ((Vector3D)b).getVz());
            return dot;
        }
        return dot;
    }

    static IVector crossProduct(IVector a, IVector b, VectorRepo repo)   {
        if(a instanceof Vector3D && b instanceof Vector3D) {

            double vx = (a.getVy() * ((Vector3D) b).getVz() - ((Vector3D) a).getVz()*b.getVy());
            double vy = (((Vector3D) a).getVz() * b.getVx() - a.getVx()*((Vector3D) b).getVz());
            double vz = (a.getVx() * b.getVy() - a.getVy()*b.getVx());

            return repo.getVector(vx, vy, vz);
        } else if (a instanceof Vector && b instanceof Vector) {

            double vz = (a.getVx() * b.getVy() - a.getVy()*b.getVx());

            return repo.getVector(0, 0, vz);
        } else return null;
    }

    static IVector addVectors(IVector a, IVector b, VectorRepo repo) {
        double vx = a.getVx() + b.getVx();
        double vy = a.getVy() + b.getVy();

        if(a instanceof Vector3D && b instanceof Vector3D)  {
            double vz = (((Vector3D) a).getVz() + ((Vector3D) b).getVz());
            return repo.getVector(vx, vy, vz);
        }
        return repo.getVector(vx, vy);
    }

    static IVector subtractVectors(IVector a, IVector b, VectorRepo repo) {
        double vx = a.getVx() - b.getVx();
        double vy = a.getVy() - b.getVy();

        if(a instanceof Vector3D && b instanceof Vector3D) {
            double vz = ((Vector3D) a).getVz() - ((Vector3D) b).getVz();
            return repo.getVector(vx, vy, vz);
        }
        return repo.getVector(vx, vy);
    }

    static IVector multiplyVectorByNumber(IVector a, double number, VectorRepo repo) {

        double vx = a.getVx() * number;
        double vy = a.getVy() * number;

        if(a instanceof Vector3D) {
            double vz = ((Vector3D) a).getVz() * number;
            return repo.getVector(vx, vy, vz);
        }
        return repo.getVector(vx, vy);
    }


}