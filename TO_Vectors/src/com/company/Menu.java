package com.company;

import java.util.Scanner;

public class Menu {

    public void menuInit(VectorRepo repo)  {

        while(true)    {
            System.out.println("Wybierz operację:");
            System.out.println("[1] - Dodanie nowego wektora 2D do repozytorium");
            System.out.println("[2] - Dodanie nowego wektora 3D do repozytorium");
            System.out.println("[3] - Usunięcie wektora z repozytorium");
            System.out.println("[4] - Iloczyn skalarny");
            System.out.println("[5] - Iloczyn wektorowy");
            System.out.println("[6] - Dodawanie wektorów");
            System.out.println("[7] - Odejmowanie wektorów");
            System.out.println("[8] - Zapis wektorów do pliku .json");
            System.out.println("[9] - Odczyt z pliku .json");
            System.out.println("[0] - Zakończenie aplikacji");


            System.out.println("Podaj nr operacji, którą chcesz wykonać:\n");
            int choice = inputInteger();

            switch (choice) {
                case 1:
                    Menu.addVector2D(repo);
                    break;

                case 2:
                    Menu.addVector3D(repo);
                    break;

                case 3:
                    Menu.removeVector(repo);
                    break;

                case 4:
                    System.out.print("Iloczyn skalarny: " + Menu.countDotProduct(repo));
                    break;

                case 5:
                    Menu.countCrossProduct(repo);
                    break;

                case 6:
                    Menu.addVectors(repo);
                    break;

                case 7:
                    Menu.subtractionVectors(repo);
                    break;

                case 8:
                    Menu.saveJSON(repo);
                    break;

                case 9:
                    Menu.readJSON(repo);
                    break;

                case 0:
                    System.exit(0);
                    break;
            }
        }
    }


    public static double inputDouble() {
        Scanner myObj = new Scanner(System.in);
        do {
            String inputString = myObj.nextLine().replace(",", ".");
            if(inputString.matches("^-?[0-9.]*")) {
                if(inputString.chars().filter(num -> num == '.').count() <= 1) {
                    if(inputString.chars().filter(num -> num == '-').count() <= 1) {
                        return Double.parseDouble(inputString);
                    }
                }
            }
            System.out.println("BŁĄD! Wprowadzono nieprawidłową wartość: " + inputString);
            System.out.println("Podaj liczbę rzeczywistą jeszcze raz:");
        } while(true);

    }

    public static int inputInteger() {
        Scanner myObj = new Scanner(System.in);
        do {
            String inputString = myObj.nextLine();
            if(inputString.matches("^-?[0-9]*")) {
                if(inputString.chars().filter(num -> num == '-').count() <= 1) {
                    return Integer.parseInt(inputString);
                }
            }
            System.out.println("BŁĄD! Wprowadzono nieprawidłową wartość: " + inputString);
            System.out.println("Podaj liczbę całkowitą jeszcze raz:");
        } while(true);

    }

    public static void addVector2D(VectorRepo repo) {

        System.out.print("Podaj współrzędne x i y wektora:\n");
        double vx = inputDouble();
        double vy = inputDouble();;
        if(!repo.doesVectorExists(vx, vy)) {
            repo.addVector(new Vector(vx, vy));
            System.out.print("Pomyślnie dodano wektor 2D do repozytorium.\n");
        } else {
            System.out.println("Ostrzeżenie: Wektor już istnieje w repozytorium!");
        }

    }

    public static void addVector3D(VectorRepo repo) {

        System.out.print("Podaj współrzędne x, y i z wektora:\n");
        double vx = inputDouble();
        double vy = inputDouble();
        double vz = inputDouble();
        if(!repo.doesVectorExists(vx, vy, vz)) {
            repo.addVector(new Vector3D(vx, vy, vz));
            System.out.print("Pomyślnie dodano wektor 3D do repozytorium.\n");
        } else {
            System.out.println("Ostrzeżenie: Wektor już istnieje w repozytorium!");
        }
    }

    public static void removeVector(VectorRepo repo)    {
        System.out.print("Czy chcesz usunąć wektor 2D czy 3D? Wybierz [2] lub [3]\n");
        int choice2 = inputInteger();
        if(choice2 == 2) {
            System.out.print("Podaj współrzędne x i y wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double [] cords = {vx, vy};
            if(repo.doesVectorExists(cords)) {
                repo.removeVector(repo.getVector(cords));
                System.out.print("Pomyślnie usunięto wektor o zadanych współrzędnych z repozytorium.\n");
            } else {
                System.out.println("Wektor o podanych współrzędnych nie istnieje w repozytorium.");
            }

        } else if(choice2 == 3)  {
            System.out.print("Podaj współrzędne x, y i z wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double vz = inputDouble();
            double [] cords = {vx, vy, vz};
            if(repo.doesVectorExists(cords)) {
                repo.removeVector(repo.getVector(cords));
                System.out.print("Pomyślnie usunięto wektor o zadanych współrzędnych z repozytorium.\n");
            } else {
                System.out.println("Wektor o podanych współrzędnych nie istnieje w repozytorium.");
            }
        }
    }

    public static double countDotProduct(VectorRepo repo)   {
        System.out.print("Czy wektory będą 2D czy 3D? Wybierz [2] lub [3]\n");
        int choice3 = inputInteger();
        if(choice3 == 2) {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double [] cords = {vx, vy};
            Vector vec1 = (Vector) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            Vector vec2 = (Vector) repo.getVector(cords);
            return IVector.dotProduct(vec1, vec2);
        } else if(choice3 == 3)  {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double vz = inputDouble();
            double [] cords = {vx, vy, vz};
            Vector3D vec1 = (Vector3D) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            vz = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            cords[2] = vz;
            Vector3D vec2 = (Vector3D) repo.getVector(cords);
            return IVector.dotProduct(vec1, vec2);
        }
        return 0;
    }

    public static IVector countCrossProduct(VectorRepo repo)    {

        System.out.print("Czy wektory będą 2D czy 3D? Wybierz [2] lub [3]\n");
        int choice3 = inputInteger();
        if(choice3 == 2) {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double [] cords = {vx, vy};
            Vector vec1 = (Vector) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            Vector vec2 = (Vector) repo.getVector(cords);
            IVector vec = IVector.crossProduct(vec1, vec2, repo);
            System.out.println("Iloczyn wektorowy:");
            vec.displayCords();
            return vec;
        } else if(choice3 == 3)  {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double vz = inputDouble();
            double [] cords = {vx, vy, vz};
            Vector3D vec1 = (Vector3D) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            vz = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            cords[2] = vz;
            Vector3D vec2 = (Vector3D) repo.getVector(cords);
            IVector vec = IVector.crossProduct(vec1, vec2, repo);
            System.out.print("Iloczyn wektorowy:");
            vec.displayCords();
            return vec;
        }

        return null;
    }

    public static IVector addVectors(VectorRepo repo)   {
        System.out.print("Czy wektory będą 2D czy 3D? Wybierz [2] lub [3]\n");
        int choice3 = inputInteger();
        if(choice3 == 2) {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double [] cords = {vx, vy};
            Vector vec1 = (Vector) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            Vector vec2 = (Vector) repo.getVector(cords);
            return IVector.addVectors(vec1, vec2, repo);
        } else if(choice3 == 3)  {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double vz = inputDouble();
            double [] cords = {vx, vy, vz};
            Vector3D vec1 = (Vector3D) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            vz = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            cords[2] = vz;
            Vector3D vec2 = (Vector3D) repo.getVector(cords);
            return IVector.addVectors(vec1, vec2, repo);
        }
        return null;
    }

    public static IVector subtractionVectors(VectorRepo repo)   {
        System.out.print("Czy wektory będą 2D czy 3D? Wybierz [2] lub [3]\n");
        int choice3 = inputInteger();
        if(choice3 == 2) {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double [] cords = {vx, vy};
            Vector vec1 = (Vector) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            Vector vec2 = (Vector) repo.getVector(cords);
            return IVector.subtractVectors(vec1, vec2, repo);
        } else if(choice3 == 3)  {
            System.out.print("Podaj współrzędne pierwszego wektora:\n");
            double vx = inputDouble();
            double vy = inputDouble();
            double vz = inputDouble();
            double [] cords = {vx, vy, vz};
            Vector3D vec1 = (Vector3D) repo.getVector(cords);
            System.out.print("Podaj współrzędne drugiego wektora:\n");
            vx = inputDouble();
            vy = inputDouble();
            vz = inputDouble();
            cords[0] = vx;
            cords[1] = vy;
            cords[2] = vz;
            Vector3D vec2 = (Vector3D) repo.getVector(cords);
            return IVector.subtractVectors(vec1, vec2, repo);
        }
        return null;
    }

    public static void saveJSON(VectorRepo repo)    {

        WriteJSON.write(repo);
        System.out.print("Wektory zostały pomyślnie zapisane do pliku .json.\n");
    }

    public static void readJSON(VectorRepo repo)    {
        ReadJSON.read(repo);
        System.out.print("Wektory zostały pomyślnie wczytane z pliku .json.\n");
    }
}