package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSON {

    public static void read(VectorRepo repo)    {

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("vectors.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray vectorsList = (JSONArray) obj;
            //System.out.println(vectorsList);

            //Iterate over employee array
            vectorsList.forEach( vector -> parseVectorObject((JSONObject)vector, repo));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void parseVectorObject(JSONObject vector, VectorRepo repo)
    {
        JSONObject vectorObject = (JSONObject) vector.get("vector");

        double vx = (double) vectorObject.get("vx");
//        System.out.println(vx);

        double vy = (double) vectorObject.get("vy");
//        System.out.println(vy);

        try {
            double vz = (double) vectorObject.get("vz");
            repo.addVector(new Vector3D(vx, vy, vz));
        } catch (RuntimeException e) {
            repo.addVector(new Vector(vx, vy));
        }
    }
}