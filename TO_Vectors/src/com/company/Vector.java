package com.company;

import java.lang.*;

public class Vector implements IVector{
    protected double vx;
    protected double vy;

    Vector() {

    }

    Vector(double vx, double vy)    {
            this.vx = vx;
            this.vy = vy;
    }

    public double getVx() {
        return vx;
    }

    public double getVy() {
        return vy;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }

    public double getAbs()  {
        return Math.sqrt(this.vx * this.vx + this.vy * this.vy);
    }

    public void displayCords() {
        System.out.println("vX = " + this.getVx());
        System.out.println("vY = " + this.getVy());
    }
}
