package com.company;

public class Vector3D implements IVector    {
    double vz;
    Vector Vector2D;

    public Vector3D(double vx, double vy, double vz)   {
        this.Vector2D = new Vector(vx, vy);
        this.vz = vz;
    }

    public double getVz()   {
        return this.vz;
    }

    public double getVx()   {
        return this.Vector2D.getVx();
    }

    public double getVy()   {
        return this.Vector2D.getVy();
    }

    public void setVx(double vx)  {
        this.Vector2D.setVx(vx);
    }

    public void setVy(double vy)  {
        this.Vector2D.setVy(vy);
    }

    public double getAbs() {
        return Math.sqrt(this.Vector2D.getVx() * this.Vector2D.getVx() + this.Vector2D.getVy() * this.Vector2D.getVy() + this.vz * this.vz);
    }

    public void displayCords() {
        System.out.println("vX = " + this.getVx());
        System.out.println("vY = " + this.getVy());
        System.out.println("vZ = " + this.getVz());
    }

}
