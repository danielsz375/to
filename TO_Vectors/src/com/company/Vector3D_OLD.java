package com.company;

import java.lang.Math;

public class Vector3D_OLD extends Vector {
    private double vz;

    public Vector3D_OLD(double vx, double vy, double vz) {
        super(vx, vy);
        this.vz = vz;
    }

    public double getVz() {
        return this.vz;
    }

    public double getAbs() {
        return Math.sqrt(this.vx*this.vx + this.vy*this.vy + this.vz*this.vz);
    }

}