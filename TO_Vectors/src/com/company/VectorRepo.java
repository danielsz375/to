package com.company;
import java.util.ArrayList;
import java.util.List;

public class VectorRepo {
    private List<IVector> vectorList;

    VectorRepo()    {
        this.vectorList = new ArrayList<>();
    }

    public boolean doesVectorExists(double vx, double vy) {
        for(IVector vector : this.vectorList) {
            if(vector instanceof Vector) {
                if(vector.getVx() == vx && vector.getVy() == vy) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean doesVectorExists(double vx, double vy, double vz) {
        for(IVector vector : this.vectorList) {
            if(vector instanceof Vector3D) {
                if(vector.getVx() == vx && vector.getVy() == vy && ((Vector3D)vector).getVz() == vz) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean doesVectorExists(double[] cords) {
        int length = cords.length;

        for(IVector vector : vectorList) {
            if(vector instanceof Vector3D && length == 3) {
                if(vector.getVx() == cords[0] && vector.getVy() == cords[1] && ((Vector3D)vector).getVz() == cords[2]) {
                    return true;
                }
            } else if (vector instanceof Vector && length == 2) {
                if(vector.getVx() == cords[0] && vector.getVy() == cords[1]) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean doesVectorExists(IVector v) {
        for(IVector vector : this.vectorList) {
            if(vector instanceof Vector3D && v instanceof Vector3D) {
                if(vector.getVx() == v.getVx() && vector.getVy() == v.getVy() && ((Vector3D)vector).getVz() == ((Vector3D)v).getVz()) {
                    return true;
                }
            } else if (vector instanceof Vector && v instanceof Vector) {
                if(vector.getVx() == v.getVx() && vector.getVy() == v.getVy()) {
                    return true;
                }
            }
        }
        return false;
    }

    private IVector createVector(double vx, double vy) {
        double cords[] = {vx, vy};
        if(doesVectorExists(cords)) {
            return getVector(cords);
        }
        return createVector(cords);
    }

    private IVector createVector(double vx, double vy, double vz) {
        double cords[] = {vx, vy, vz};
        if(doesVectorExists(cords)) {
            return getVector(cords);
        }
        return createVector(cords);
    }

    private IVector createVector(double[] cords) {
        int length = cords.length;
        if(length == 2 || length == 3) {
            if(doesVectorExists(cords)) {
                return getVector(cords);
            } else {
                IVector newVector;
                if(length == 2) {
                    newVector = new Vector(cords[0], cords[1]);
                    addVector(newVector);
                    return newVector;
                } else if(length == 3) {
                    newVector = new Vector3D(cords[0], cords[1], cords[2]);
                    addVector(newVector);
                    return newVector;
                }
            }
        } else {
            System.out.println("BŁĄD: Podano niewłaściwą ilość współrzędnych!");
        }
        return null;
    }

    public void addVector(IVector v) {
        this.vectorList.add(v);

        System.out.println("Do repozytorium dodano wektor o współrzędnych: ");
        System.out.println("vX = " + v.getVx());
        System.out.println("vY = " + v.getVy());

        if(v instanceof Vector3D) {
            System.out.println("vZ = " + ((Vector3D)v).getVz());
        }
    }

    public void removeVector(IVector v) {
        if(doesVectorExists(v)) {
            this.vectorList.remove(v);
        } else System.out.println("");

    }

    public IVector getVector(double vx, double vy) {
        double cords[] = {vx, vy};
        return getVector(cords);
    }

    public IVector getVector(double vx, double vy, double vz) {
        double cords[] = {vx, vy, vz};
        return getVector(cords);
    }

    public IVector getVector(double[] cords) {
        double length = cords.length;

        for(IVector vector : vectorList) {
            if(vector instanceof Vector3D && length == 3) {
                if(vector.getVx() == cords[0] && vector.getVy() == cords[1] && ((Vector3D)vector).getVz() == cords[2]) {
                    return vector;
                }
            } else if (vector instanceof Vector && length == 2) {
                if(vector.getVx() == cords[0] && vector.getVy() == cords[1]) {
                    return vector;
                }
            }
        }

        if(length == 2 || length == 3) {
            System.out.println("Ostrzeżenie: Wektor o podanych współrzędnych nie został znaleziony w repozytorium.");
            return createVector(cords);
        }


        System.out.println("Niewłaściwa ilość współrzędnych wektora: " + length);
        return null;
    }

    public List<IVector> getVectorList() {
        return this.vectorList;
    }
}