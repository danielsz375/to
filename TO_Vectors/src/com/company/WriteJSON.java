package com.company;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public abstract class WriteJSON {

    public static void write(VectorRepo repo)   {

        JSONArray vectorsList = new JSONArray();

        for(IVector vector : repo.getVectorList()) {
            JSONObject vectorDetails = new JSONObject();
            vectorDetails.put("vx", vector.getVx());
            vectorDetails.put("vy", vector.getVy());
            if (vector instanceof Vector3D) {
                vectorDetails.put("vz", ((Vector3D) vector).getVz());
            }

            JSONObject vectorObject = new JSONObject();
            vectorObject.put("vector", vectorDetails);

            vectorsList.add(vectorObject);
        }


        //Write JSON file
        try (FileWriter file = new FileWriter("vectors.json")) {

            file.write(vectorsList.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

